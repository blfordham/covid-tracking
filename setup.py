import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
     name='covidtracking',  
     version='0.1',
     author="Bryan L. Fordham",
     author_email="bryan@nativesavannah.com",
     description="Simple API to get data from https://covidtracking.com",
     long_description=long_description,
   long_description_content_type="text/markdown",
     url="https://gitlab.com/blfordham/covid-tracking",
     packages=setuptools.find_packages('./src'),
     package_dir={'covidtracking': 'src/covidtracking',},
     classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
 )