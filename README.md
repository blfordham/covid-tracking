# Covid Tracking API

Simple API to get data from https://covidtracking.com

```python
>>> import sys
>>> sys.path.append('src')
>>> from covidtracking import CovidTracking
>>> c = CovidTracking()
>>> x = c.states() # Get all states
>>> ga = c.states('GA') # Get only Georgia data
>>> twoStates = c.states('GA', 'SC') # You can pass multiple filters
```

The data is cached, since it's not updated constantly. To refresh, call `clear()` first.
